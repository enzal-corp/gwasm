
# GWASM

## Introduction
GWASM is a project that might end up becoming a two player P2P game that you can play in your browser (hence the name G for game and WASM for... yeah WASM).
For the moment only the communication between the two peers is done.
The two peers communicate through a WebRTC datachannel (UDP) but they need the help of a handshake server to connect to each other.

## Notes
* The handshake server and the client use different libraries to handle websockets.
  It's because I started with the one from gorilla wich doesn't support WebAssembly.
  I'm planning to fully switch to the other one soon thought.
* It's a personal project so the comments might not be sufficient.

## Build instructions
(Tested only on Linux)

First you'll need to compile the client(s) in WASM.
Just run this command from the project's root.
```
GOOS=js GOARCH=wasm && go build -o ./build/wasm/host.wasm ./src/client/host.go && go build -o ./build/wasm/guest.wasm ./src/client/guest.go
```
Then you can compile the server :
```
GOOS= GOARCH= && go build -o ./build/web_server ./src/web-server/
```

## Test the program
To launch the server just run :
```
./build/web_server
```
Then open your browser and go to localhost:8000/host/hostname (hostname can be changed).
Open a new tab and go to localhost:8000/guest/hostname.
Press F12 to bring up the console, you should see messages coming from the host.
The server isn't needed anymore so you can stop it and see the messages still coming.
