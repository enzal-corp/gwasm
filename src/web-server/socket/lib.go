package socket

import (
	"github.com/gorilla/websocket"
)

type Peer struct {
	conn        *websocket.Conn
	candidates  [][]byte
	sessionDesc []byte
}

func (peer *Peer) AddCandidate(c []byte) {
	peer.candidates = append(peer.candidates, c)
}

func (peer *Peer) SendSession(recipient *Peer) error {
	if peer.sessionDesc != nil {
		err := recipient.conn.WriteMessage(websocket.TextMessage, peer.sessionDesc)
		return err
	}
	return nil
}

func (peer *Peer) SendLastCandidate(recipient *Peer) error {
	if len(peer.candidates) != 0 {
		err := recipient.conn.WriteMessage(websocket.TextMessage, peer.candidates[len(peer.candidates)-1])
		return err
	}
	return nil
}

func (peer *Peer) SendAllCandidates(recipient *Peer) error {
	for _, c := range peer.candidates {
		err := recipient.conn.WriteMessage(websocket.TextMessage, c)
		return err
	}
	return nil
}



type Room struct {
	host  Peer
	guest Peer
}

func NewRoom(hostConn *websocket.Conn) *Room {
	return &Room{
		host:  Peer{hostConn, [][]byte{}, []byte{}},
		guest: Peer{},
	}
}