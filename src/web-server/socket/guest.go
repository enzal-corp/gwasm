package socket

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"strings"
)

func GuestWsHandler(w http.ResponseWriter, r *http.Request){
	hostname := mux.Vars(r)["hostname"]
	if _, found := roomMap[hostname]; !found {
		_, _ = fmt.Fprintf(os.Stderr, "Hostname %s non existent\n", hostname)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	host := &roomMap[hostname].host
	guest := &roomMap[hostname].guest
	guest.conn = conn
	if err = host.SendSession(guest); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Couldn't send session through websocket: ", err.Error())
	}
	if err = host.SendAllCandidates(guest); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Couldn't send candidates through websocket: ", err.Error())
	}

	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "Couldn't read host message: ", err.Error())
			break
		}

		// Sort messages and send them to the host
		str := string(msg)
		if strings.HasPrefix(str, "{\"candidate\":") {
			guest.AddCandidate(msg)
			if err := guest.SendLastCandidate(host); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate to guest Peer: ", err.Error())
			}
		} else if strings.HasPrefix(str, "{\"type\":") {
			guest.sessionDesc = msg
			if err = guest.SendSession(host); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "Couldn't send session through websocket: ", err.Error())
			}
		}
	}
}
