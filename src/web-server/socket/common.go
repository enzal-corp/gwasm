package socket

import "github.com/gorilla/websocket"

var roomMap = make(map[string]*Room)
var upgrader =  websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024, CheckOrigin: nil}
