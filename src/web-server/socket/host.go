package socket

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"strings"
)

// todo : this function shares a lot of code with GuestWsHandler(), refactoring needed
func HostWsHandler(w http.ResponseWriter, r *http.Request){
	hostname := mux.Vars(r)["hostname"]
	if _, found := roomMap[hostname]; found {
		_, _ = fmt.Fprintf(os.Stderr, "Hostname %s already taken\n", hostname)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// As soon as the connection is closed the server can remove the room
	// because it's only used for the handshake between the two peers
	conn.SetCloseHandler(func(_ int, _ string) error {
		println("Room closed: ", hostname)
		delete(roomMap, hostname)
		return nil
	})
	defer conn.Close()

	roomMap[hostname] = NewRoom(conn)
	host := &roomMap[hostname].host
	guest := &roomMap[hostname].guest

	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "Couldn't read host message:", err.Error())
			break
		}

		// Sort messages and send them to the guest / store them if he's not connected yet
		str := string(msg)
		if strings.HasPrefix(str, "{\"candidate\":") {
			host.AddCandidate(msg)
			// todo: candidate could be sent 2 times in extremely improbable cases
			if guest.conn != nil {
				if err := host.SendLastCandidate(guest); err != nil {
					_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate to guest Peer: ", err.Error())
				}
			}
		} else if strings.HasPrefix(str, "{\"type\":") {
			host.sessionDesc = msg
			if guest.conn != nil {
				if err = host.SendSession(guest); err != nil {
					_, _ = fmt.Fprintln(os.Stderr, "Couldn't send session through websocket: ", err.Error())
				}
			}
		}
	}
}
