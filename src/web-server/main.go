package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"gwasm/src/web-server/socket"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"
)

type HTMLServer struct {
	server *http.Server
	wg     sync.WaitGroup
}

func main() {
	host := "localhost:8000"
	htmlServer := start(host, 5 * time.Second)
	defer func(htmlServer *HTMLServer) {
		if err := htmlServer.stop(); err != nil {
			_, _ = fmt.Fprintln(os.Stderr, err.Error())
		}
	}(htmlServer)

	// Wait for a signal interrupt
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan

	println("Main : shutting down")
}

func start(host string, timeout time.Duration) *HTMLServer {
	router := initRouter()

	htmlServer := HTMLServer{
		server: &http.Server{
			Addr:           host,
			Handler:        router,
			ReadTimeout:    timeout,
			WriteTimeout:   timeout,
			MaxHeaderBytes: 1 << 20,
		},
	}
	htmlServer.wg.Add(1)

	go func() {
		println("\nHTMLServer : Service started : Host= ", host)
		htmlServer.server.ListenAndServe()
		htmlServer.wg.Done()
	}()

	return &htmlServer
}

func (htmlServer *HTMLServer) stop() error {
	const timeout = 5 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	println("\nHTMLServer : Service stopping")

	if err := htmlServer.server.Shutdown(ctx); err != nil {
		if err := htmlServer.server.Close(); err != nil {
			println("\nHTMLServer : Service stopping : Error=", err.Error())
			return err
		}
	}

	// Wait for the server to finish ListenAndServe() before terminating program
	htmlServer.wg.Wait()
	println("\nHTMLServer : Stopped")
	return nil
}

func initRouter() *mux.Router {
	router := mux.NewRouter()
	fileServer := http.FileServer(NeuteredFileSystem{Fs: http.Dir("./wasm")})


	router.HandleFunc("/host/{hostname}", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./static/host.html")
	})
	router.HandleFunc("/guest/{hostname}", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./static/guest.html")
	})
	router.HandleFunc("/host/{hostname}/ws", socket.HostWsHandler)
	router.HandleFunc("/guest/{hostname}/ws", socket.GuestWsHandler)
	router.PathPrefix("/fs/").Handler(http.StripPrefix("/fs/", fileServer))

	return router
}

