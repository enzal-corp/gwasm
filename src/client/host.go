package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pion/webrtc/v3"
	"gwasm/src/client/rtc"
	"nhooyr.io/websocket"
	"os"
	"strings"
	"time"
)

func main() {
	ctx := context.Background()
	conn, peerConnection, err := rtc.Init()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Couldn't initialize ws conn / peer conn: ", err.Error())
	}
	defer rtc.ClosePeerConn(peerConnection)

	// Start a goroutine to read and treat incoming messages on the websocket
	go handleHostMessages(conn, peerConnection, ctx)

	// Create a datachannel and send a message every 5 sec. (for demonstration purposes)
	datachannel, err := peerConnection.CreateDataChannel("data", nil)
	if err != nil {
		panic(err)
	}
	datachannel.OnOpen(func() {
		for {
			if err := datachannel.SendText("Hi there :D"); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "Couldn't send message through datachannel")
			}
			time.Sleep(time.Second * 5)
		}
	})

	// Without a valid offer we can't establish a connection, hence the usage of panic()
	offer, err := peerConnection.CreateOffer(nil)
	if err != nil {
		panic(err)
	}
	// This will start to gather the ICE candidates and start our UDP listeners
	_ = peerConnection.SetLocalDescription(offer)

	marshalledOffer, err := json.Marshal(offer)
	if err != nil {
		panic(err)
	}
	if err = conn.Write(ctx, websocket.MessageText, marshalledOffer); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Couldn't send offer through websocket")
	}

	select {}
}

func handleHostMessages(conn *websocket.Conn, peer *webrtc.PeerConnection, ctx context.Context) {
	var candidates [][]byte

	for {
		_, msg, err := conn.Read(ctx)
		if err != nil {
			return
		}

		str := string(msg)
		if strings.HasPrefix(str, "{\"candidate\":") {
			switch peer.RemoteDescription() {
			case nil:
				candidates = append(candidates, msg)
			default:
				if err := rtc.AddCandidate(msg, peer); err != nil {
					_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate: ", err.Error())
				}
			}
		} else if strings.HasPrefix(str, "{\"type\":\"answer\"") {
			if err := rtc.SetSession(msg, peer); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate: ", err.Error())
			}

			for _, c := range candidates {
				if err := rtc.AddCandidate(c, peer); err != nil {
					_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate: ", err.Error())
				}
			}
			candidates = nil
		}
	}
}
