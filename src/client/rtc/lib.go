package rtc

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pion/webrtc/v3"
	"nhooyr.io/websocket"
	"os"
	"syscall/js"
)

func Init() (*websocket.Conn, *webrtc.PeerConnection, error) {
	ctx := context.Background()
	config := webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	}

	url := js.Global().Get("window").Get("location").Get("href").String()
	conn, _, err := websocket.Dial(ctx, "ws" + url[4:] + "/ws", nil)
	if err != nil {
		return nil, nil, err
	}

	peerConnection, err := webrtc.NewPeerConnection(config)
	if err != nil {
		return nil, nil, err
	}


	// When an ICE candidate is available send it to the server for the handshake
	peerConnection.OnICECandidate(func(cand *webrtc.ICECandidate) {
		if cand == nil {
			return
		}

		marshalledCand, _:= json.Marshal(cand.ToJSON())
		if err := conn.Write(ctx, websocket.MessageText, marshalledCand); err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "Couldn't send candidate through websocket")
		}
	})

	// Close the connection with the server as soon as p2p is established
	peerConnection.OnConnectionStateChange(func(s webrtc.PeerConnectionState) {
		// If the connection state isn't connecting or connected the peers are not
		// connected anymore, so we can terminate the program
		switch s {
		case webrtc.PeerConnectionStateConnecting :
			println("Peers connecting")
		case webrtc.PeerConnectionStateConnected :
			println("Peers connected")
			_ = conn.Close(websocket.StatusNormalClosure, "p2p connection established")
		default:
			println("Goodbye")
			os.Exit(1)
		}
	})

	return conn, peerConnection, nil
}

func AddCandidate(marshaledCandidate []byte , peer *webrtc.PeerConnection) error {
	var candidate webrtc.ICECandidateInit
	json.Unmarshal(marshaledCandidate, &candidate)
	err := peer.AddICECandidate(candidate)
	if err != nil {
		return err
	}
	return nil
}

func SetSession(marshaledSesssion []byte, peer *webrtc.PeerConnection) error {
	var session webrtc.SessionDescription
	err := json.Unmarshal(marshaledSesssion, &session)
	err = peer.SetRemoteDescription(session)
	if err != nil {
		return err
	}
	return nil
}

func ClosePeerConn(peer *webrtc.PeerConnection) {
	if err := peer.Close(); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "cannot close peerConnection: ", err.Error())
	}
}
