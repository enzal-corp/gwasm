package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pion/webrtc/v3"
	"gwasm/src/client/rtc"
	"nhooyr.io/websocket"
	"os"
	"strings"
)

func main() {
	conn, peerConnection, err := rtc.Init()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Couldn't initialize ws conn / peer conn: ", err.Error())
	}
	defer rtc.ClosePeerConn(peerConnection)

	// Start a goroutine to read and treat incoming messages on the websocket
	go handleGuestMessages(conn, peerConnection)

	// Print messages sent by the host when they arrive
	peerConnection.OnDataChannel(func(d *webrtc.DataChannel) {
		d.OnMessage(func(msg webrtc.DataChannelMessage) {
			println(string(msg.Data))
		})
	})

	select {}
}

func handleGuestMessages(conn *websocket.Conn, peer *webrtc.PeerConnection) {
	var candidates [][]byte
	ctx := context.Background()

	for {
		_, msg, err := conn.Read(ctx)
		if err != nil {
			return
		}

		str := string(msg)
		if strings.HasPrefix(str, "{\"candidate\":") {
			switch peer.RemoteDescription() {
			case nil:
				candidates = append(candidates, msg)
			default:
				if err := rtc.AddCandidate(msg, peer); err != nil {
					_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate: ", err.Error())
				}
			}
		} else if strings.HasPrefix(str, "{\"type\":\"offer\"") {
			if err := rtc.SetSession(msg, peer); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "Couldn't add candidate: ", err.Error())
			}

			for _, c := range candidates {
				if err := rtc.AddCandidate(c, peer); err != nil {
					_, _ = fmt.Fprintln(os.Stderr, "Couldn't set session: ", err.Error())
				}
			}
			candidates = nil

			answer, err := peer.CreateAnswer(nil)
			if err != nil {
				panic(err)
			}
			_ = peer.SetLocalDescription(answer)

			// Send the answer to the server for the handshake
			marshalledAnswer, _ := json.Marshal(answer)
			if err = conn.Write(ctx, websocket.MessageText, marshalledAnswer); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "Couldn't send answer through websocket")
			}
		}
	}
}

